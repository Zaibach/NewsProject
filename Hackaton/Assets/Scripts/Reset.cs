﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Reset : MonoBehaviour 
{
    public GameObject refSlot;  //variabile di referenza per la slot da svuotare
    public GameObject articleToReset;  //variabile di referenza per l'articolo da riposizionare
    public DragAndDrop refScript; //variabile di referenza per lo script da utilizzare

    public void AsignRef() //chiamato al drop sulla slot di referenza,assegna le altre ref
    {
        articleToReset = refSlot.transform.GetChild(1).gameObject; //assegna l'articolo
        refScript = articleToReset.GetComponent<DragAndDrop>(); //assegna lo script
    }

    public void reset() //chiamato quando si preme il bottone
    {
        articleToReset.SetActive(true); //riattiva l'articolo cosi che sia di nuovo visibile
        articleToReset.GetComponent<CanvasGroup>().blocksRaycasts = true; // rende possibili interagici
        articleToReset.transform.position = refScript.startPos; //lo riporta alla posizione originale
        articleToReset.transform.SetParent(refScript.startParent); //lo assegna al padre iniziale 
        refSlot.GetComponentInChildren<Text>().text = ""; //cancella il testo nella casella
        this.gameObject.SetActive(false);// fa sparire il pulsante
    }


}