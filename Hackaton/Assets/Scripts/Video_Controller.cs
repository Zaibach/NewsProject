﻿using UnityEngine;
using System.Collections;

public class Video_Controller : MonoBehaviour
{

    public GameObject info;
    bool IsPlay = true;
    GameObject[] videos;
    GameObject current;


    public void PlayVideo()
    {
        videos = GameObject.FindGameObjectsWithTag("video");

        for (int i = 0; i < videos.Length; i++)
        {
            if (videos[i].activeSelf)
                current = videos[i];
        }

        if (!((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).isPlaying)
        {
            RepeatVideo();
            ((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).Play();         
        }
    }

    public void PauseVideo()
    {
        videos = GameObject.FindGameObjectsWithTag("video");

        for (int i = 0; i < videos.Length; i++)
        {
            if (videos[i].activeSelf)
                current = videos[i];
        }


        if (((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).isPlaying)
        {
            ((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).Pause();
            IsPlay = false;
        }
        else if (!IsPlay)
        {
            ((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).Play();
            IsPlay = true;
        }
    }

    public void RepeatVideo()
    {
        videos = GameObject.FindGameObjectsWithTag("video");

        for (int i = 0; i < videos.Length; i++)
        {
            if (videos[i].activeSelf)
                current = videos[i];
        }


        ((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).Stop();
        current.GetComponent<AudioSource>().Stop();
        ((MovieTexture)current.GetComponent<CanvasRenderer>().GetMaterial().mainTexture).Play();
        current.GetComponent<AudioSource>().Play();
    }

    public void getInfo()
    {
        if(info.active)
           info.gameObject.SetActive(false);
        else
            info.gameObject.SetActive(true);

    }
}
