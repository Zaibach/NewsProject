﻿using UnityEngine;
using System.Collections;

public class Event_System : MonoBehaviour
{
    public GameObject canvasVideo;
    public GameObject canvasGiornale;
    public GameObject confirmPannel;

    public void Impaginazione ()
    {
        canvasVideo.SetActive(false);
        canvasGiornale.SetActive(true);
    }
    public void Conferma()
    {
        confirmPannel.SetActive(true);
    }
    public void Annulla()
    {
        confirmPannel.SetActive(false);
    }
}
