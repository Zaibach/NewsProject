﻿using UnityEngine;
using System.Collections;

public class Selection : MonoBehaviour {

    // Use this for initialization
    public GameObject video;

    GameObject[] videos;
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void selectVideo()
    {

        videos = GameObject.FindGameObjectsWithTag("video");

        for (int i = 0; i < videos.Length; i++)
        {
            if (videos[i].activeSelf)
            {
                ((MovieTexture)videos[i].GetComponent<CanvasRenderer>().GetMaterial().mainTexture).Stop();
                videos[i].SetActive(false);
            }
        }
        video.SetActive(true);

    }
}
