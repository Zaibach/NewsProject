﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour,IBeginDragHandler,IDragHandler, IEndDragHandler {

    static public GameObject ItemDragged; //variabile di riferimento per l'ogetto trascinato,torna null una volta lasciato
    public Vector3 startPos; //Posizione iniziale del ItemDragged,serve per riposizionare gli ogetti col reset o quando vengono trascinati in spazi non validi
    static public int point; //valore in punti della notizia
    public Transform startParent; //la casella di partenza dell'articolo
    static public string finalTitle; // la dicitura che apparirà nella casella finale*forse va cambiata in un immagine
    public string titleIstance; // istanza per la variabile statica qui sopra*come sopra
    public int pointInstance; //istanza per la variabile statica dei punti
    public GameObject gm; //reference al Game Manager

    public void OnBeginDrag(PointerEventData eventData) //chiamata quando inizi il drag
    {
        ItemDragged = gameObject; //setta la variabile sull'oggetto draggato
        startPos = transform.position; //setta la var di posizione su quella della dell'oggetto
        startParent = transform.parent; //setta il padre
        GetComponent<CanvasGroup>().blocksRaycasts = false; //fa in modo che  l'oggetto possa essere trascinato tramite raycast del mouse
        finalTitle = titleIstance; //setta la variabile statica uguale alla relativa istanza dell'oggetto
        point = pointInstance; // idem come opra ma per i punti
    } 
    public void OnDrag(PointerEventData eventData) //everytick fino a quando stai draggando
    {
        transform.position = Input.mousePosition; //trascina
    }
    public void OnEndDrag(PointerEventData eventData) // si attva al drop ma solo fuori dalle slot
    {

       
        ItemDragged = null; //azzera la variabile relativa all'ogett trascinato
        GetComponent<CanvasGroup>().blocksRaycasts = true; // riabilità il raycast per prendere altri oggetti
        if (transform.parent == startParent) 
        {
            transform.position = startPos;  //se non è stato assegnato a un'altra slot torna al suo posto originale
        }
    }
}
