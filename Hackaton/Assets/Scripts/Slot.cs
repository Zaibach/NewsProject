﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour,IDropHandler {
    public int multiplyer; //moltiplcatore punti
    public int _point; //forse non servirà a niente ma l'ho lasciata perchè può essere utile per dare info al GM...
    public Reset myReset;//bottone di reset di referenza
    public string tag;

    public GameObject article //articolo assegnato,è sempre il secondo figlio della slot(il primo è il testo)quindi non aggiungere altri figli,o bisogna modificare lo script
    {
        get
        {
            if (transform.childCount == 1)
            {
                return transform.GetChild(1).gameObject;
            }
            return null;
        }

    }
	public void OnDrop(PointerEventData eventData)//chiamata quando un oggetto è droppato nella slot
    {
        if (!article && DragAndDrop.ItemDragged.tag == tag)
        {
            DragAndDrop.ItemDragged.transform.SetParent(transform); //assegna l'oggetto come figlio
            //gameObject.GetComponentInChildren<Text>().text =GetComponentInChildren<DragAndDrop>().titleIstance; //rende il testo della slot uguale a quello immeso nell'ispector
            _point =(multiplyer * DragAndDrop.point); //calcola i punti totali della casella
            myReset.AsignRef(); //chiama la funzione che asegna le referenze per il reset
            article.SetActive(false); // fa sparire l'articolo origniale
            myReset.gameObject.SetActive(true); //fa appparire il tasto di reset
        }
    }
}
